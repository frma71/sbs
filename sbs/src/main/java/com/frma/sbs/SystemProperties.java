package com.frma.sbs;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by frma on 2017-01-27.
 */

class SystemProperties {
    static Method mPropGet;
    static boolean initialized = false;
    static private void init() {
        Class c = null;
        try {
            c = Class.forName("android.os.SystemProperties");
            mPropGet = c.getMethod("get", String.class);
            initialized = true;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
    static public String get(String prop) {
        if(!initialized)
            init();
        try {
            return (String)mPropGet.invoke(false, prop);
        } catch (IllegalAccessException e) {
            return null;
        } catch (InvocationTargetException e) {
            return null;
        }
    }
    static public String getSOName() {
        String product = get("ro.build.product");
        String version = get("ro.build.version.release");
        String cmdev = SystemProperties.get("ro.cm.device");

        L.d("ro.build.version.release: " + version);
        L.d("ro.cm.device: " + cmdev);
        L.d("ro.build.product: " + product);

        if(cmdev != null && !cmdev.equals(""))
            product = "cm_" + product;

        String sofile = "libsurfaceflinger_" + product + "_" + version + ".so";

        return sofile;
    }
}
