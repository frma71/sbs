package com.frma.sbs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.ViewFlipper;
import android.text.method.LinkMovementMethod;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.stericson.RootShell.RootShell;

public class MainActivity extends Activity implements
        View.OnClickListener,
        SeekBar.OnSeekBarChangeListener, CompoundButton.OnCheckedChangeListener {
    private Button mLoadBtn;
    private TextView mCurrentStatusTV;
    private ToggleButton mActivateTB;
    private SeekBar mZoomSeekBar;
    private TextView mZoomFactor;
    private SeekBar mImgDistSeekBar;
    private TextView mImgDistValue;

    private ProgressDialog mProgress;

    private boolean mLoaded;
    private boolean mActive;
    private int mZoom;
    private int mImgDist;
    private BroadcastReceiver mBReceiver;
    private ViewFlipper mViewFlipper;
    private static final int VIEW_SPLASH = 0;
    private static final int VIEW_LOAD = 1;
    private static final int VIEW_INCOMPATIBLE = 2;
    private static final int VIEW_OPERATE = 3;
    private static final int VIEW_NOROOT = 4;
    private int currentView = 0;
    private Tracker mTracker;
    private boolean mIsCompatible;

    String getViewName(int view) {
        if(view == VIEW_SPLASH) return "splash";
        if(view == VIEW_LOAD) return "load";
        if(view == VIEW_INCOMPATIBLE) return "incompatible";
        if(view == VIEW_OPERATE) return "operate";
        if(view == VIEW_OPERATE) return "noroot";
        return "UNKNOWN";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        L.d("onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SBSApplication app = (SBSApplication)getApplication();
        mTracker = app.getDefaultTracker();

        String sofile = SystemProperties.getSOName();
        TextView incompatibleTV = (TextView)findViewById(R.id.incompatibleText);
        incompatibleTV.setText(incompatibleTV.getText().toString().replace("XXXSOFILEXXX", sofile.substring(18,sofile.length()-3)));
        incompatibleTV.setMovementMethod(LinkMovementMethod.getInstance());


        ((TextView)findViewById(R.id.versionText)).setText("Version: " + getVersionName());

        mViewFlipper = (ViewFlipper)findViewById(R.id.viewFlipper);
        mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_in_from_right));
        mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_out_to_left));

        if(MyService.showAds() && GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this) != ConnectionResult.SUCCESS) {
            Toast.makeText(this, "You need google play services", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        // Get screen width and height
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        mLoadBtn = (Button)findViewById(R.id.load);
        mCurrentStatusTV = (TextView)findViewById(R.id.activeStatus);
        mActivateTB = (ToggleButton)findViewById(R.id.activateTB);
        mZoomSeekBar = (SeekBar) findViewById(R.id.zoomSeekBar);
        mZoomFactor = (TextView) findViewById(R.id.zoomValue);

        mImgDistSeekBar = (SeekBar) findViewById(R.id.marginSeekBar);
        mImgDistValue = (TextView) findViewById(R.id.marginValue);

        mLoadBtn.setOnClickListener(this);
        mActivateTB.setOnCheckedChangeListener(this);

        mZoomSeekBar.setOnSeekBarChangeListener(this);

        mImgDistSeekBar.setOnSeekBarChangeListener(this);
        mImgDistSeekBar.setMax((int) ((float) displayMetrics.heightPixels / displayMetrics.ydpi * 25.4));

        mProgress = new ProgressDialog(this);
        mProgress.setMessage("Configuring...");

        startService(new Intent(this, MyService.class));
    }
    @Override
    protected void onResume() {
        L.d("onResume");
        super.onResume();

        mBReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(MyService.SBS_NEW_STATUS.equals(intent.getAction())) {
                    handleStatusIntent(intent);
                }
            }
        };
        IntentFilter filter = new IntentFilter();
        filter.addAction(MyService.SBS_NEW_STATUS);
        registerReceiver(mBReceiver, filter);

        MyService.ctrlSBS(this, "ping");
        mProgress.show();
    }

    @Override
    protected void onPause() {
        L.d("onPause");
        super.onPause();
        if(mBReceiver != null)
            unregisterReceiver(mBReceiver);
    }

    @Override
    protected void onDestroy() {
        L.d("onDestroy");
        super.onDestroy();
        if(mProgress != null && mProgress.isShowing())
            mProgress.dismiss();
    }

    private boolean mInItemUpdate = false;

    private void handleStatusIntent(Intent intent) {
        mProgress.dismiss();
        L.i("Handle status intent");
        mLoaded    = intent.getBooleanExtra("LOADED", false);
        mActive    = intent.getBooleanExtra("ACTIVE", false);
        mZoom      = intent.getIntExtra("ZOOM", 100);
        mImgDist   = intent.getIntExtra("IMGDIST", 60);
        mIsCompatible = intent.getBooleanExtra("COMPATIBLE", false);
        L.d("Compatible: " + mIsCompatible);
        updateUI();
    }

    private void setCurrentView(int view) {
        if(view != currentView) {
            currentView = view;
            mViewFlipper.setDisplayedChild(currentView);
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("Action")
                    .setAction(getViewName(currentView))
                    .setLabel(SystemProperties.getSOName())
                    .build());
        }
    }
    private String getVersionName() {
        String ver = null;
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            ver = pInfo.versionName + (MyService.showAds() ? "-ad" : "-pro");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return ver;
    }
    private void updateUI() {
        if(!mIsCompatible) {
            setCurrentView(VIEW_INCOMPATIBLE);
            return;
        }

        if(!mLoaded) {
            setCurrentView(VIEW_LOAD);
            return;
        }

        setCurrentView(VIEW_OPERATE);
        mInItemUpdate = true;
        String statusText = "SBS v" + getVersionName();
        statusText += (mLoaded ? " is loaded" +
                        (mActive  ? " and active " : " but not active")
                                    : " is not loaded");
        mCurrentStatusTV.setText(statusText);

        mZoomSeekBar.setProgress(mZoom);
        mImgDistSeekBar.setProgress(mImgDist);
        mActivateTB.setChecked(mActive);
        mInItemUpdate = false;
    }
    private boolean checkRoot() {
        if(!RootShell.isRootAvailable() || RootShell.isAccessGiven()) {
            setCurrentView(VIEW_NOROOT);
            return false;
        }
        return true;
    }
    @Override
    public void onClick(View v) {
        if(v.equals(mLoadBtn) && checkRoot()) {
                showLoadDlg();
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
        if (seekBar == mImgDistSeekBar) {
            mImgDistValue.setText("" + progress + " mm");
        }
        else if(seekBar == mZoomSeekBar) {
            mZoomFactor.setText("" + 100*progress/255 + "%");
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if(mInItemUpdate)
            return;
        if (seekBar == mImgDistSeekBar) {
            mProgress.show();
            MyService.setSBS(this, mActive, mZoom, seekBar.getProgress());
        }
        else if(seekBar == mZoomSeekBar) {
            mProgress.show();
            MyService.setSBS(this, mActive, seekBar.getProgress(), mImgDist);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void showLoadDlg() {
        if(!checkRoot())
            return;
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Load SBS  and restart UI ?")
                .setMessage("Do you want to load SBS support and restart the UI of the device")
                .setPositiveButton("Go", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mProgress.show();
                        MyService.ctrlSBS(MainActivity.this, "load");
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();
    }
    private void showUnloadDlg() {
        if(!checkRoot())
            return;
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Unload SBS and restart UI ?")
                .setMessage("Do you want to unload SBS support and restart the UI of the device")
                .setPositiveButton("Go", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mProgress.show();
                        MyService.ctrlSBS(MainActivity.this, "unload");
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();
    }
    private void showRebootDlg() {
        if(!checkRoot())
            return;
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Reboot device ?")
                .setMessage("Are you sure you want to reboot this device")
                .setPositiveButton("Go", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mProgress.show();
                        MyService.ctrlSBS(MainActivity.this, "reboot");
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();
    }
    private void showRestartDlg() {
        if(!checkRoot())
            return;
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Restart UI ?")
                .setMessage("Are you sure you want to restart the UI")
                .setPositiveButton("Go", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mProgress.show();
                        MyService.ctrlSBS(MainActivity.this, "restart");
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();
    }
;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.unload) {
            showUnloadDlg();
        }
        else if(id == R.id.reboot) {
            showRebootDlg();
        }
        else if(id == R.id.restart) {
            showRestartDlg();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        L.d("onCheckedChanged");
        if(buttonView.equals(mActivateTB) && buttonView.isPressed()) {
            mProgress.show();
            MyService.setSBS(this, isChecked, mZoom, mImgDist);
        }
    }
}
