package com.frma.sbs;

import android.util.Log;

class L {
    private static String tag() {
        return new Exception().getStackTrace()[2].getClassName();
    }
    static void v(String msg) {
        Log.v(tag(), msg);
    }
    static void i(String msg) {
        Log.i(tag(), msg);
    }
    static void d(String msg) {
        Log.d(tag(), msg);
    }
    static void w(String msg) {
        Log.w(tag(), msg);
    }
    static void e(String msg) {
        Log.e(tag(), msg);
    }
}