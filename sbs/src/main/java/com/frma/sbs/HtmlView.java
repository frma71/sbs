package com.frma.sbs;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.AttributeSet;
import android.widget.TextView;

public class HtmlView extends TextView {
    public HtmlView(Context context, @Nullable AttributeSet attrs) {

        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.HtmlView, 0, 0);
        try {
            boolean isHtml = a.getBoolean(R.styleable.HtmlView_isHtml, false);
            if (isHtml) {
                String text = a.getString(R.styleable.HtmlView_android_text);
                if (text != null) {
                    setText(Html.fromHtml(text));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            a.recycle();
        }
    }
}
