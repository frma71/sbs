package com.frma.sbs;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.OrientationEventListener;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.stericson.RootShell.RootShell;
import com.stericson.RootShell.exceptions.RootDeniedException;
import com.stericson.RootShell.execution.Command;
import com.stericson.RootShell.execution.Shell;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.TimeoutException;

class Installer {
    private final Context mContext;
    String cmdResp;
    private Shell mRootShell;
    Installer(Context context) {
        mContext = context;
    }
    private void mktoast(String s) {
        Toast.makeText(mContext, s, Toast.LENGTH_LONG).show();
    }
    public boolean install(String so) {
        int rv;
        String path = mContext.getFilesDir() + "/" + so;
        String dst = "/data/local/tmp/libsurfaceflinger.so";
        rv = runAsRoot("cp " + path + " " + dst);
        if(rv != 0) {
            mktoast("Failed to copy file from assets");
            return false;
        }
        rv = runAsRoot("chmod  644 " + dst);
        if(rv != 0) {
            mktoast("Failed to chane mode of file");
            return false;
        }
        rv = runAsRoot("chcon u:object_r:system_file:s0 " + dst);
        if(rv != 0) {
            mktoast("Failed to change security context of file");
            return false;
        }
        rv = runAsRoot("mount -o bind " + dst + " /system/lib64/libsurfaceflinger.so");
        if(rv != 0) {
            mktoast("Failed to bind mount libsurfaceflinger");
            return false;
        }
        restart();
        return true; // Never reached
    }
    public boolean uninstall() {
        String dst = " /system/lib64/libsurfaceflinger.so";
        int rv = runAsRoot("umount -l " + dst);
        if(rv != 0) {
            mktoast("Failed to unmount libsurfaceflinger");
            return false;
        }
        restart();
        return true; // Never reached
    }
    public void reboot() {
        int rv = runAsRoot("reboot");
    }
    public void restart() {
        int rv = runAsRoot("(stop ; start) &");
    }
    private int runAsRoot(String cmd) {
        L.i("Run as root: " + cmd);
        RootShell.debugMode = false;

        try {
            if(mRootShell == null)
                mRootShell = RootShell.getShell(true);
            L.i("Got root shell");
        } catch (IOException | TimeoutException | RootDeniedException e) {
            mktoast("Failed to get root");
            return -1;
        }

        Command rcmd = new Command(0, false, cmd) {
            public String resp;
            @Override
            public void commandOutput(int id, String line)
            {
                cmdResp += line;
                super.commandOutput(id, line);
            }
        };

        int rv = -1;
        String msg = "";
        try {
            cmdResp = "";
            mRootShell.add(rcmd);
            while (!rcmd.isFinished())
                Thread.sleep(10);
            rv = rcmd.getExitCode();
            msg = cmdResp;
            L.i("runAsRoot returned " + rv);
        } catch (InterruptedException e) {
            L.i("Failed to run as root with exception: " + e.getMessage());
            Toast.makeText(mContext, "Failed to run as root", Toast.LENGTH_LONG).show();
            msg = "Exception while executing external command:" + e.getMessage();
        } catch (IOException e) {
            L.i("Failed to run as root with exception: " + e.getMessage());
            Toast.makeText(mContext, "Failed to run as root", Toast.LENGTH_LONG).show();
            msg = "Exception while executing external command:" + e.getMessage();
        }
        return rv;
    }
}

class SurfaceFlinger {
    IBinder mSurfaceFlinger;
    SurfaceFlinger() {
    }
    boolean init() {
        Class c = null;
        try {
            c = Class.forName("android.os.ServiceManager");
            L.d("Class is " + c);
            Method m = c.getMethod("getService", String.class);
            L.d("Method is " + m);
            Object o = m.invoke(null, "SurfaceFlinger");
            L.d("Object is " + o);
            mSurfaceFlinger = (IBinder)o;
            L.d("IB is " + mSurfaceFlinger);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            return false;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return false;
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            return false;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    CallReturn call(int flags, float zoom, float eyeDist) {
        Parcel in = Parcel.obtain();
        Parcel out = Parcel.obtain();
        CallReturn r = null;
        try {
            in.writeInterfaceToken("android.ui.ISurfaceComposer");
            in.writeInt(flags);
            in.writeFloat(zoom);
            in.writeFloat(eyeDist);
            mSurfaceFlinger.transact(4711,in, out, 0);
            r = new CallReturn(out.readInt(),
                    out.readFloat(),
                    out.readFloat());
            L.d("Flags: " + r.flags);
            L.d("Zoom: " + r.zoom);
            L.d("Dist: " + r.eyeDist);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        }
        in.recycle();
        out.recycle();
        return r;
    }
    class CallReturn {
        public CallReturn(int flags, float zoom, float eyeDist) {
            this.flags = flags;
            this.zoom = zoom;
            this.eyeDist = eyeDist;
        }
        public int flags;
        public float zoom;
        public float eyeDist;
    }
};

public class MyService extends Service {
    static final String SBS_CTRL = "com.frma.sbs.action.SBS_CTRL";
    static final String SBS_SET  = "com.frma.sbs.action.SBS_SET";
    static final String SBS_NEW_STATUS = "com.frma.sbs.action.STATUS";
    private DisplayMetrics mDisplayMetrics;
    private SharedPreferences mPrefs;
    private boolean mIsLandscape;
    private boolean mIsOn;
    private Handler mPollHandler;
    private boolean mIsLoaded;
    private SurfaceFlinger mSurfaceFlinger;
    private Installer mInstaller;
    private boolean mIsCompatible;
    InterstitialAd mInterstitialAd;

    public static boolean showAds() {
        return !BuildConfig.FLAVOR.equals("paid");
    }

    public static void setSBS(Context context, boolean on, int zoom, int imgDistance) {
        Intent intent = new Intent(context, MyService.class);
        intent.setAction(SBS_SET);
        intent.putExtra("ON", on);
        intent.putExtra("ZOOM", zoom);
        intent.putExtra("IMGDIST", imgDistance);
        context.startService(intent);
    }
    public static void ctrlSBS(Context context, String action) {
        Intent intent = new Intent(context, MyService.class);
        intent.setAction(SBS_CTRL);
        intent.putExtra("CMD", action);
        context.startService(intent);
    }
    public MyService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mSurfaceFlinger = new SurfaceFlinger();
        mInstaller = new Installer(this);
        if(!mSurfaceFlinger.init()) {
            Toast.makeText(this, "Failed to connect to surfaceflinger",
                    Toast.LENGTH_LONG).show();
            android.os.Process.killProcess(android.os.Process.myPid());
        }

        // Run in foreground so we don't get killed
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        Notification notification = new Notification.Builder(this)
                .setContentTitle("SBS")
                .setContentText("SBS is running")
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(1, notification);

        // Setup for ads and start loading
        if(showAds()) {
            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId("ca-app-pub-5560511718039518/4216526987");
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    L.d("onAdLoaded");
                    super.onAdLoaded();
                }

                @Override
                public void onAdClosed() {
                    L.d("onAdClosed");
                    requestNewInterstitial();
                }
            });
            requestNewInterstitial();
        }

        // Copy assets to files directory
        copyAssets("armeabi");

        // Check if our hardware is ssupported
        String sofile = SystemProperties.getSOName();
        File f = new File(getFilesDir(), sofile);
        mIsCompatible = f.exists();

        // Get display metrics
        WindowManager window = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = window.getDefaultDisplay();
        mDisplayMetrics = new DisplayMetrics();
        display.getMetrics(mDisplayMetrics);

        // Get preference manager
        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        final OrientationEventListener mOrientationListener = new OrientationEventListener(this) {
            @Override
            public void onOrientationChanged(int o) {
                if (!mIsOn || o == ORIENTATION_UNKNOWN)
                    return;
                if (!mIsLandscape && o > 270 - 30 && o < 270 + 30) {
                    L.w("Landscape mode detected " + o);
                    mIsLandscape = true;
                    handleSBSSet(true, 0, 0);
                } else if (mIsLandscape && !(o > 270 - 45 && o < 270 + 45)) {
                    L.w("Exited landscape mode  " + o);
                    mIsLandscape = false;
                    boolean on = mPrefs.getBoolean("ON", false);
                    handleSBSSet(true, 0, 0);
                }
            }
        };
        mOrientationListener.enable();

        mPollHandler = new Handler();
        mPollHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mIsOn && mIsLandscape) {
                    if (showAds() && mInterstitialAd.isLoaded())
                        mInterstitialAd.show();
                }
                mSurfaceFlinger.call((mIsOn & mIsLandscape) ? 1 : 0, 0.0f, 0.0f);
                mPollHandler.postDelayed(this, 3 * 60 * 1000);
            }
        }, 3 * 60 * 1000);
    }

    private void requestNewInterstitial() {
        L.d("requestNewInterstitial");
        AdRequest adRequest = new AdRequest.Builder()
                //.addTestDevice("4CF689FC73B600A8A31C4AA2DC5329D1")
                .build();
        mInterstitialAd.loadAd(adRequest);
    }

    @Override
    public void onDestroy() {
        stopForeground(true);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int a, int b) {
        if (intent != null) {
            final String action = intent.getAction();
            if (SBS_CTRL.equals(action)) {
                String cmd = intent.getStringExtra("CMD");
                handleSBSControl(cmd);
            } else if (SBS_SET.equals(action)) {
                boolean on = intent.getBooleanExtra("ON", mPrefs.getBoolean("ON", false));
                int zoom = intent.getIntExtra("ZOOM", mPrefs.getInt("zoom", 100));
                int imgDistance = intent.getIntExtra("IMGDIST", mPrefs.getInt("imgdist", 60));
                handleSBSSet(on, zoom, imgDistance);
            }
        }
        return START_STICKY;
    }

    // Asset stuff
    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }

    private void copyAssets(String path) {
        AssetManager assetManager = getAssets();
        String[] files = null;
        try {
            files = assetManager.list(path);
        } catch (IOException e) {
            L.e("Failed to get asset file list.");
        }
        for(String filename : files) {
            InputStream in = null;
            OutputStream out = null;
            try {

                in = assetManager.open(path + "/" + filename);
                File outFile = new File(getFilesDir(), filename);
                out = new FileOutputStream(outFile);
                L.d("copy " + outFile);
                copyFile(in, out);
                in.close();
                in = null;
                out.flush();
                out.close();
                out = null;
                Runtime.getRuntime().exec( "chmod 755 " + outFile.getAbsolutePath());
            } catch(IOException e) {
                L.e("Failed to copy asset file: " + filename);
            }
        }
    }
    private void handleSBSControl(String cmd) {
        String o;
        if (cmd.equals("load")) {
            mInstaller.install(SystemProperties.getSOName());
        } else if (cmd.equals("unload")) {
            mInstaller.uninstall();
        } else if (cmd.equals("reboot")) {
            mInstaller.reboot();
            //noinspection InfiniteLoopStatement
            while(true);
        } else if (cmd.equals("restart")) {
            mInstaller.restart();
            //noinspection InfiniteLoopStatement
            while(true);
        }
        sendNewStatusIntent();
    }
    private void handleSBSSet(boolean on, int zoom, int imgDistance) {
        mIsOn = on;
        if(zoom != 0)
            mPrefs.edit().putInt("zoom" ,zoom).apply();
        if(imgDistance != 0)
            mPrefs.edit().putInt("imgdist", imgDistance).apply();

        SurfaceFlinger.CallReturn cr =
                mSurfaceFlinger.call((on & mIsLandscape) ? 1 : 0,
                    (float)zoom/255.0f,
                    (float)(imgDistance / 25.4 * mDisplayMetrics.xdpi)/mDisplayMetrics.heightPixels);
        sendNewStatusIntent();
    }

    private void sendNewStatusIntent() {
        int zoom = mPrefs.getInt("zoom", 200);
        int imgDistance = mPrefs.getInt("imgdist", 60);
        boolean err = false;
        Intent intent = new Intent(SBS_NEW_STATUS);

        L.d("Send new status intent");
        String o = "";
        if(!mIsLoaded) {
            SurfaceFlinger.CallReturn cr = mSurfaceFlinger.call(-1 ,0, 0);
            if(cr != null) {
                mIsLoaded = true;
                mIsOn = (cr.flags & 1) == 1;
                zoom = (int)(255*cr.zoom);
                imgDistance = (int)(cr.eyeDist * 25.4 * mDisplayMetrics.heightPixels / mDisplayMetrics.xdpi);
            }
            else {
                mIsLoaded = false;
            }
        }
        if(!err) {
            intent.putExtra("LOADED", mIsLoaded);
            intent.putExtra("ACTIVE", mIsOn);
            intent.putExtra("ZOOM", zoom);
            intent.putExtra("IMGDIST", imgDistance);
            intent.putExtra("COMPATIBLE", mIsCompatible);
            L.d("Compatible: " + mIsCompatible);
        } else {
            intent.putExtra("ERROR_MSG", o);
        }
        getApplicationContext().sendBroadcast(intent);
    }
    public static String readAll(InputStream stream) throws IOException, UnsupportedEncodingException {
        int n = 0;
        char[] buffer = new char[1024 * 4];
        InputStreamReader reader = new InputStreamReader(stream, "UTF8");
        StringWriter writer = new StringWriter();
        while (-1 != (n = reader.read(buffer))) writer.write(buffer, 0, n);
        return writer.toString();
    }
}
